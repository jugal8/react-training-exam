import "./App.css";
import Routing from "./component/Routing";
import AuthenticationProvider from "./context/Auth-context";

const App = () => {
  return (
    <AuthenticationProvider>
      <Routing />
    </AuthenticationProvider>
  );
};

export default App;
