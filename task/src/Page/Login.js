import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import {useContext} from 'react'
import { useNavigate } from 'react-router-dom';
import classes from './Login.module.css';
import { Authentication } from '../context/Auth-context';



// console.log(Authentication,'sdcsd')
const SignupSchema = Yup.object().shape({
    password: Yup.string()
        // .matches(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
        //     "Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and one special case Character"
        // )
        .required('Required'),
    email: Yup.string().email('Invalid email').required('Required'),
});

const Login = ({ setUser }) => {

    const {add}=useContext(Authentication)

    const navigate = useNavigate();
    return (
        <div>

            <h1 className={classes.heading}>Login</h1>
            <Formik
                initialValues={{
                    email: '',
                    password: '',

                }}
                validationSchema={SignupSchema}
                onSubmit={values => {
                    // localStorage.setItem('email', values.email);
                    // localStorage.setItem('password', values.password);
                    add(values.email,values.password);
                    navigate("/dashbord");
                    // setUser(values.email, values.password);
                    // data(values.email, values.password);

                }}
            >
                {({ errors, touched }) => (
                    <Form className={classes.card}>
                        <div className='mb-4'>
                            <label className="form-label ">Email address </label>
                            <Field name="email" type="email" placeholder='Email' className={`col-11 ${classes.inputField} `} />
                            {errors.email && touched.email ?
                                <div className={classes.error}>{errors.email}</div>
                                : null}
                        </div>

                        <div className='mb-4'>
                            <label className="form-label">Password </label>
                            <Field name="password" type='password' placeholder='Password' className={`col-11 ${classes.inputField} `} />
                            {errors.password && touched.password ? (
                                <div className={classes.error}>{errors.password}</div>
                            ) : null}
                        </div>

                        <button className="btn btn-primary mb-3">login</button>

                    </Form>
                )}
            </Formik>
        </div>
    );
};

export default Login;