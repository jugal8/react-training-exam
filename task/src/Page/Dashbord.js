import axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";
import { useState } from "react";
import Header from "../component/Header/Header";
import './Dashbord.css';
import client from '../component/axios'
const apiKey = "119cebc0785867cce21b8c9421697fd9";
// const weather = "https://i.pinimg.com/originals/77/0b/80/770b805d5c99c7931366c2e84e88f251.png";

function NewHome() {
    const [inputCity, setInputCity] = useState("");
    const [data, setData] = useState(null);
    const [error, setError] = useState(null);
    const [loading, setLoading]=useState(false)

    console.log('url',process.env.REACT_APP_URL)
    const getWetherDetails = (cityName) => {
        if (!cityName) return;
        setLoading(true)
        const apiURL =` https://api.openweathermap.org/data/2.5/forecast?q= ${cityName}&appid=${apiKey}`;
       
        axios.get(apiURL).then((res) => {
            setData(res.data)
        }).then(setLoading(false)) 
        .catch((err) => {
            setError(err.response.data.message);
        });
        
    };

    const handleChangeInput = (e) => {
        setInputCity(e.target.value);
    };


const HandleKey=(event)=>{
    if(event.key === 'Enter'){
        getWetherDetails(inputCity)
      }
    
}

    return (
        <>
            <Header />
            <div className="col-md-12">
                <div className="wetherBg">
                    <h1 className="heading">Weather App</h1>

                    <div className="d-grid gap-3 col-4 mt-4">
                        <input type="text"
                            className="form-control"
                            value={inputCity}
                            onChange={handleChangeInput} 
                            onKeyPress={HandleKey}/>
                       
                    </div>
                </div>

               
     {data?(    
        <div className='main'>
                { data && data.list.splice(0,4).map((item,index)=>{
                return(
                    
                   <div className=' shadow rounded wetherResultBox oneline'> 
                    {console.log(item)}
                    <div>{item?.dt_txt} </div><hr/>
                    <img className='weathorIcon' src={`http://openweathermap.org/img/w/${item.weather[0].icon}.png`}/>
                   <div> {item.weather[0].description} </div>
                   
                 
                      <h6 className="weathorTemp">
                                  {((item?.main?.temp) - 273.15).toFixed(2)}°C
                              </h6>
                              <div>
                                  <span >Min:
                                      {((item?.main?.temp_min) - 273.15).toFixed(2)}°C
                                  </span>
                                  <span className="ms-4">Max:
                                      {((item?.main?.temp_max) - 273.15).toFixed(2)}°C
                                  </span>
                              </div>
                              <hr/>
                              <h5 className='weathorTemp'> Visibility </h5>
                                <h5> {item.visibility} </h5>
                              <hr />
                              <h6 className="weathorTemp">Humidity </h6>
                              <h5>{(item?.main?.humidity)} %</h5>
                              <hr />
                              {/* convert into miles */}
                              <h6 className="weathorTemp">Visibility </h6>
                              <h5>{((item?.visibility) * 0.00062)} Miles</h5>
                              <hr />
                              <h6 className="weathorTemp">Pressure </h6>
                              <h5>{(item?.main?.pressure)} mb</h5>
  
                  </div>  ) }
               

                  
                )
                }
                </div>
            ):(<h1>  {error}</h1>)  
     }
        </div>
        </>);
}

export default NewHome;