import React from 'react';
import classes from './NotFound.module.css';
const Notfound = () => {
    return (
        <div className="container">
            <div className="row">
                <div className="col-md-12">
                    <div className={classes.errortemplate}>
                        <h1>
                            Oops!</h1>
                        <h2>
                            404 Not Found</h2>
                        <div >
                            Sorry, an error has occured, Requested page not found!
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Notfound;