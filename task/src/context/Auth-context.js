import { createContext, useState } from "react";

export const Authentication = createContext();

const AuthenticationProvider = props => {
  const [user, setUser] = useState([{ email: "a@1.com", password: "aA23" }]);

  const add = (email, password) => {
    setUser([{ email: email, password: password }]);
  };
  return (
    <Authentication.Provider value={{ user, add }}>
      {props.children}
    </Authentication.Provider>
  );
};
export default AuthenticationProvider;
