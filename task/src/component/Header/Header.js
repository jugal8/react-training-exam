import React, { useContext } from "react";
import { useNavigate } from "react-router-dom";
import { Authentication } from "../../context/Auth-context";
import classes from "./Header.module.css";

// import {Authentication} from '../../context/Auth-context';
const Header = () => {
  const { user } = useContext(Authentication);
  console.log("sdfcsd", user[0].email);
  console.log(Authentication);
  // const user = useContext(Authentication);
  const navigate = useNavigate();
  const email = localStorage.getItem("email");

  const HandleLogOut = () => {
    localStorage.clear();
    navigate("/");
  };

  return (
    <nav className="navbar bg-light">
      <div className="container-fluid">
        <div className="navbar-brand">
          {user[0].email}
        </div>
        <button
          className={` btn btn-outline-success ${classes.button} `}
          onClick={HandleLogOut}
        >
          Logout
        </button>
      </div>
    </nav>
  );
};

export default Header;
