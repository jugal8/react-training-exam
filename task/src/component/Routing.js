import React from 'react';
import { Routes, Route } from 'react-router-dom';
import Login from '../Page/Login.js';
import Dashbord from '../Page/Dashbord.js';
import Notfound from '../Page/Notfound.js';

const Routing = () => {
  return (

    <Routes>

      <Route path='/' element={<Login />} />
      <Route path='/dashbord' element={<Dashbord />} />
      <Route path='*' element={<Notfound />} />

    </Routes>

  );
};

export default Routing;



//try for making route Protected

// {
//   !localStorage.getItem('email') &&
//   <Route path='/' element={<Login />} />;
// }

// {
//   localStorage.getItem('email') &&
//   <Route path='/home' element={<NewHome />}/>;
// }